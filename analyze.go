package main

import (
    "io"
    "io/ioutil"
    "path/filepath"
)

type Analyzer interface {
    analyze(writer io.Writer)
}

type TypescriptAnalyzer struct {
    index *Index
}

func newTypescriptAnalyzer(index *Index) Analyzer {
    return &TypescriptAnalyzer{
        index,
    }
}

func (a *TypescriptAnalyzer) analyze(writer io.Writer) {
    cw := NewClassWriter(a.index, writer)
    cw.Write()

    writeTypescriptPackages(a.index, writer)
}

func createIndex(path string) *Index {
    infos, err := ioutil.ReadDir(path)
    showErrorAndExit(err)
    index := newIndex()
    for _, fi := range infos {
        if fi.IsDir() {
            continue
        }
        if filepath.Ext(fi.Name()) != ".json" || fi.Name() == "apiindex.json" || fi.Name() == "apidata.json" {
            continue
        }
        index.addClass(filepath.Join(path, fi.Name()))
    }
    return index
}
