package main

import (
    "fmt"
    "io"
    "strings"
)

type ClassWriter interface {
    Write()
}

type TypescriptClassWriter struct {
    index     *Index
    writer    io.Writer
    currClass *Class
}

func NewClassWriter(index *Index, writer io.Writer) ClassWriter {
    return &TypescriptClassWriter{
        index,
        writer,
        nil,
    }
}

func (w *TypescriptClassWriter) Write() {
    for _, cls := range w.index.orderedClasses {
        w.currClass = cls
        w.writeClass()
    }
}

func (w *TypescriptClassWriter) writeClass() {
    w.expandMixins()
    w.writeStaticMembers()
    w.writeInstanceMembers()
}

func (w *TypescriptClassWriter) expandMixins() {
    clsData := w.currClass.data
    mixins := clsData.Attributes.Str("mixins")
    if mixins == "" {
        return
    }

    clsMethods := clsData.FindChildEntity("methods")
    if clsMethods == nil {
        return
    }

    for _, mixinName := range strings.Split(mixins, ",") {
        mixin, ok := w.index.classes[mixinName]
        if !ok {
            panic(fmt.Errorf("Can't find mixin '%s' while parsing class '%s'", mixinName, w.currClass.getFqn()))
        }
        mixinData := mixin.data
        for _, methodList := range mixinData.ExtractChildren("methods") {
            for _, method := range methodList.Children {
                clsMethods.Children = append(clsMethods.Children, method)
            }
        }
    }
}

func (w *TypescriptClassWriter) writeStaticMembers() {
    fmt.Fprintf(w.writer, "interface %s {\n", w.currClass.intermediateName(true))
    for _, _type := range []string{"methods-static", "constants", "constructor"} {
        childs := w.currClass.data.ExtractChildren(_type)
        w.writeMembers(true, childs)
    }
    fmt.Fprintf(w.writer, "}\n")
}

func (w *TypescriptClassWriter) writeInstanceMembers() {
    if parent, ok := w.getSuperClass(); ok {
        fmt.Fprintf(w.writer, "interface %s extends %s {\n", w.currClass.intermediateName(false), parent)
    } else {
        fmt.Fprintf(w.writer, "interface %s {\n", w.currClass.intermediateName(false))
    }
    childs := w.currClass.data.ExtractChildren("methods")
    w.writeMembers(false, childs)
    fmt.Fprintf(w.writer, "}\n")
}

func (w *TypescriptClassWriter) writeMembers(static bool, members Children) {
    for _, childs := range members {
        for _, child := range childs.ExtractChildren("") {
            w.writeMember(static, child)
        }
    }
}

func (w *TypescriptClassWriter) writeMember(static bool, member *Entity) {
    switch {
    case static && !member.Attributes.Bool("isStatic") && !member.Attributes.Bool("isCtor"):
        return
    case !static && member.Attributes.Bool("isStatic"):
        return
    }

    var paramRoot *Entity = nil
    for _, p := range member.ExtractChildren("params") {
        paramRoot = p
        break
    }
    returns := member.ExtractChildren("return")
    var returnTypes []string
    if member.Attributes.Bool("isCtor") {
        returnTypes = append(returnTypes, w.currClass.intermediateName(false))
    } else {
        returnTypes = w.collectTypesFrom(returns)
    }

    if len(returnTypes) > 1 {
        for i, v := range returnTypes {
            if v == "void" {
                copy(returnTypes[i:], returnTypes[i+1:])
                returnTypes = returnTypes[:len(returnTypes)-1]
                break
            }
        }
    }

    name := "new"
    if !member.Attributes.Bool("isCtor") {
        name = member.Attributes.Str("name")
    }

    var params Children
    if paramRoot != nil {
        params = paramRoot.ExtractChildren("")
    }
    if len(params) > 0 {
        var paramTypes []Attributes
        for _, v := range params {
            types := w.collectTypes(v)
            types = uniquify(types)
            if len(types) == 0 {
                types = append(types, "any")
            }
            paramTypes = append(paramTypes, Attrs("name", v.Attributes["name"], "types", types))
        }
        for _, permutation := range w.possiblePermutations(paramTypes, 0) {
            var args []Attributes
            for permIndex, param := range paramTypes {
                args = append(args, Attrs("name", param["name"],
                    "type", param["types"].([]string)[permutation[permIndex]]))
            }
            w.writeFunc(name, args, returnTypes)
        }
    } else {
        w.writeFunc(name, nil, returnTypes)
    }
}

func (w *TypescriptClassWriter) possiblePermutations(types []Attributes, paramIndex int) (res [][]int) {
    param := types[paramIndex]
    for typeIndex := range param["types"].([]string) {
        if paramIndex+1 == len(types) {
            res = append(res, []int{typeIndex})
        } else {
            for _, v := range w.possiblePermutations(types, paramIndex+1) {
                res = append(res, append([]int{typeIndex}, v...))
            }
        }
    }
    return
}

func (w *TypescriptClassWriter) writeFunc(name string, args []Attributes, returnTypes []string) {
    fmt.Fprintf(w.writer, "\t%s(", name)
    sArgs := make([]string, 0, len(args))
    for _, a := range args {
        sArgs = append(sArgs, a["name"].(string)+": "+a["type"].(string))
    }
    fmt.Fprintf(w.writer, "%s): %s;\n", strings.Join(sArgs, ", "), returnTypes[0])
}

func (w *TypescriptClassWriter) collectTypesFrom(items Children) (res []string) {
    for _, i := range items {
        res = append(res, w.collectTypes(i)...)
    }
    if len(res) == 0 {
        res = append(res, "void")
    }
    return uniquify(res)
}

func (w *TypescriptClassWriter) collectTypes(e *Entity) (res []string) {
    for _, types := range e.ExtractChildren("types") {
        for _, _type := range types.ExtractChildren("") {
            res = append(res, w.getIntermediateType(_type.Attributes["type"].(string)))
        }
    }
    return res
}

func (w *TypescriptClassWriter) getIntermediateType(_type string) string {
    _type = normalizeType(_type, w.currClass, w.index)
    if strings.Contains(_type, ".") {
        if t, ok := w.index.classes[_type]; ok {
            return t.intermediateName(false)
        }
        _type = "any"
    }
    return _type
}

func (w *TypescriptClassWriter) getSuperClass() (string, bool) {
    if p, ok := w.currClass.data.Attributes["superClass"]; ok {
        parent := w.getIntermediateType(p.(string))
        if parent == "any" {
            return "", false
        }
        if strings.HasSuffix(parent, "[]") {
            return "Array", true
        }
        return parent, true
    }
    return "", false
}

func uniquify(data []string) (res []string) {
    set := make(map[string]bool)
    for _, v := range data {
        set[v] = true
    }
    res = make([]string, 0, len(set))
    for k := range set {
        res = append(res, k)
    }
    return
}
