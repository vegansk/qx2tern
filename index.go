package main

import (
    "fmt"
    "strings"
)

type IndexItem struct {
    pkg  []string
    name string
}

func newIndexItem(pkg []string) *IndexItem {
    i := &IndexItem{}
    l := len(pkg)
    switch l {
    case 0:
        // NOP
    case 1:
        i.name = pkg[0]
    default:
        i.pkg = pkg[:l-1]
        i.name = pkg[l-1]
    }
    return i
}

func (i *IndexItem) intermediateName(isStatic bool) (res string) {
    if len(i.pkg) > 0 {
        res = strings.Join(i.pkg, "_") + "_"
    }
    res += i.name
    if isStatic {
        res += "Static"
    }
    return
}

func (i *IndexItem) getFqn() (res string) {
    if len(i.pkg) > 0 {
        res = strings.Join(i.pkg, ".") + "."
    }
    res += i.name
    return
}

type Package struct {
    IndexItem
    contents map[string]interface{}
}

func newPackage(pkg []string) *Package {
    return &Package{
        *newIndexItem(pkg),
        make(map[string]interface{}),
    }
}

func newRootPackage() *Package {
    return newPackage(nil)
}

type Class struct {
    IndexItem
    fqn  string
    data *Entity
}

func (c *Class) getType() string {
    return c.data.Attributes.Str("type")
}

func newClass(data *Entity) *Class {
    fqn := data.Attributes["fullName"].(string)
    return &Class{
        *newIndexItem(strings.Split(fqn, ".")),
        fqn,
        data,
    }
}

type Index struct {
    orderedClasses []*Class
    classes        map[string]*Class
    packages       *Package
    typeIndex      map[string][]*Class
}

func newIndex() *Index {
    return &Index{
        nil,
        make(map[string]*Class),
        newRootPackage(),
        make(map[string][]*Class),
    }
}

func (i *Index) addClass(filePath string) {
    data, err := LoadEntityFromFile(filePath)
    showErrorAndExit(err)
    cls := newClass(data)
    pkg := i.getPackage(cls.pkg)
    pkg.contents[cls.name] = *cls
    i.classes[cls.fqn] = cls
    i.orderedClasses = append(i.orderedClasses, cls)
    i.typeIndex[cls.name] = append(i.typeIndex[cls.name], cls)
}

func (i *Index) getPackage(pkg []string) *Package {
    pkgs := i.packages
    for i, p := range pkg {
        var curr interface{}
        var ok bool
        if curr, ok = pkgs.contents[p]; !ok {
            curr = newPackage(pkg[:i+1])
            pkgs.contents[p] = curr
        }
        if _, ok = curr.(*Package); !ok {
            showErrorAndExit(fmt.Errorf("Package/Type conflict: %s", strings.Join(pkg[:i+1], ".")))
        }
        pkgs = curr.(*Package)
    }

    return pkgs
}
