package main

import (
    "flag"
    "fmt"
    "io"
    "os"
)

func showErrorAndExit(err interface{}) {
    if err == nil {
        return
    }
    fmt.Fprintln(os.Stderr, err)
    os.Exit(-1)
}

func exitHandler() {
    err := recover()
    if err != nil {
        showErrorAndExit(err)
    }
}

func main() {
    //defer exitHandler()

    var ts bool
    var err error
    fl := flag.NewFlagSet("*", flag.ContinueOnError)
    fl.BoolVar(&ts, "ts", false, "Usage: qx2tern [-ts] qooxdooDir [generatedFile]")
    fl.Parse(os.Args[1:])
    var args = fl.Args()

    if len(args) < 1 {
        panic(fmt.Errorf("Usage: qx2tern [-ts] qooxdooDir [generatedFile]"))
    }

    dir := args[0]
    var writer io.Writer
    if len(args) >= 2 {
        writer, err = os.Create(args[1])
        showErrorAndExit(err)
        defer writer.(*os.File).Close()
    } else {
        writer = os.Stdout
    }
    a := newTypescriptAnalyzer(createIndex(dir))
    a.analyze(writer)
}
