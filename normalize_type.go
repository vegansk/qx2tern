package main

import (
    "regexp"
    "strings"
)

const object = " object"

func normalizeType(_type string, rootItem *Class, index *Index) string {
    objLastIdx := strings.LastIndex(_type, object)
    if objLastIdx > -1 && objLastIdx == len(_type)-len(object) {
        return _type[:len(_type)-len(object)]
    }
    _type = regexp.MustCompile(`\.{2,}`).ReplaceAllString(_type, "")
    switch _type {
    case "Class", "Clazz":
        return "qx.Class"
    case "win":
        return "Window"
    case "IframeElement", "Iframe":
        return "HTMLIFrameElement"
    case "xhr", "Xhr":
        return "XMLHttpRequest"
    case "Array":
        return "any[]"
    case "document", "DOMDocument":
        return "Document"
    case "DOMelement", "element", "DOMElement", "Element":
        return "Element"
    case "node", "DOMNode":
        return "Node"
    case "String":
        return "string"
    case "function":
        return "Function"
    case "Number", "Float", "Integer", "PositiveInteger", "PositiveNumber", "zIndex":
        return "number"
    case "exception":
        return "Error"
    case "Regexp":
        return "RegExp"
    case "Boolean", "boolean", "true", "false", "Boolen":
        return "bool"
    }
    if _type == "" || _type == "Stub" || _type == "Any" || _type == "Map" || _type == "map" || _type == "Object" || _type == "object" || _type == "varargs" || _type == "Arguments" || _type == "arguments" || _type == "Obj" || _type == "var" || _type == "Var" || _type == "Null" || _type == "null" || regexp.MustCompile(`[^\w._$]`).MatchString(_type) {
        return "any"
    }
    if index != nil {
        if _, ok := index.classes[_type]; !ok || strings.Index(_type, ".") < 0 {
            pkgs := strings.Split(_type, ".")
            name, pkgs := pkgs[len(pkgs)-1], pkgs[:len(pkgs)-1]
            ok := true
            var item interface{}
            // Get the package of the type that this type is relative to and absolutize it
            if rootItem != nil {
                pkg := index.getPackage(rootItem.pkg)

                for _, pkgName := range pkgs {
                    if item, ok = pkg.contents[pkgName]; ok {
                        pkg = item.(*Package)
                        continue
                    }
                    break
                }
                if ok {
                    if item, ok = pkg.contents[name]; ok {
                        return item.(Class).fqn
                    }
                }
            }
            // Find all types of the given name and pick the best match
            if typeIdx, ok := index.typeIndex[_type]; ok {
                var result *Class
                for _, item := range typeIdx {
                    if strings.LastIndex(item.fqn, _type) != (len(item.fqn) - len(_type)) {
                        continue
                    }
                    if result == nil || (rootItem != nil && strings.Index(item.fqn, strings.Join(rootItem.pkg, ".")) == 0) {
                        result = item
                    }
                }
                if result != nil {
                    return result.fqn
                }
            }
        }
    }
    return _type
}
