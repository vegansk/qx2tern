package main

import (
    "fmt"
    "io"
)

func writeTypescriptPackages(index *Index, writer io.Writer) {
    var write func(*Package)
    write = func(p *Package) {
        for _, item := range p.contents {
            if pkg, ok := item.(*Package); ok {
                write(pkg)
            }
        }

        _, err := writer.Write([]byte(fmt.Sprintf("interface %s {\n", p.intermediateName(true))))
        showErrorAndExit(err)
        for name, item := range p.contents {
            switch item := item.(type) {
            case Class:
                _, err = writer.Write([]byte(fmt.Sprintf("\t%s: %s;\n", name, item.intermediateName(true))))
                showErrorAndExit(err)
            case *Package:
                _, err = writer.Write([]byte(fmt.Sprintf("\t%s: %s;\n", name, item.intermediateName(true))))
                showErrorAndExit(err)
            default:
                panic(fmt.Errorf("Unknown type %T", item))
            }
        }
        _, err = writer.Write([]byte("}\n"))
        showErrorAndExit(err)
    }

    for name, item := range index.packages.contents {
        if p, ok := item.(*Package); ok {
            write(p)
        }

        switch item := item.(type) {
        case Class:
            _, err := writer.Write([]byte(fmt.Sprintf("declare var %s: %s;\n", name, item.intermediateName(true))))
            showErrorAndExit(err)
        case *Package:
            _, err := writer.Write([]byte(fmt.Sprintf("declare var %s: %s;\n", name, item.intermediateName(true))))
            showErrorAndExit(err)
        default:
            panic(fmt.Errorf("Unknown type %T", item))
        }
    }
}
