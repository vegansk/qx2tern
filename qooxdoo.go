package main

import (
    "bytes"
    "encoding/json"
    "fmt"
    "io"
    "os"
    "strings"
)

type Attributes map[string]interface{}

type Children []*Entity

type Type string

type Entity struct {
    Attributes Attributes `json:"attributes,omitempty"`
    Children   Children   `json:"children,omitempty"`
    Type       Type       `json:"type"`
}

func LoadEntityFromFile(fileName string) (*Entity, error) {
    fi, err := os.Open(fileName)
    if err != nil {
        return nil, err
    }
    defer fi.Close()

    return LoadEntity(fi)
}

func LoadEntity(reader io.Reader) (*Entity, error) {
    dec := json.NewDecoder(reader)
    e := Entity{}
    if err := dec.Decode(&e); err != nil {
        return nil, err
    }
    return &e, nil
}

func (e *Entity) ExtractChildren(filter string) (res Children) {
    if filter == "" {
        res = make(Children, 0, len(e.Children))
    }
    for _, v := range e.Children {
        if filter == "" || filter == string(v.Type) {
            res = append(res, v)
        }
    }
    return
}

func (e *Entity) FindChildEntity(_type string) *Entity {
    for _, ch := range e.Children {
        if ch.Type == Type(_type) {
            return ch
        }
    }
    return nil
}

func (e *Entity) String() string {
    b := &bytes.Buffer{}
    fmt.Fprintf(b, "{type: %s, attrs: %v, childs: %v}", e.Type, e.Attributes, e.Children)
    return b.String()
}

func NewAttrubutes(vals map[string]interface{}) Attributes {
    return Attributes(vals)
}

func Attrs(attrs ...interface{}) Attributes {
    if len(attrs)%2 != 0 {
        panic(fmt.Errorf("Can't create attributes from odd parameters count"))
    }
    res := make(map[string]interface{})
    for idx := 0; idx < len(attrs); idx += 2 {
        res[attrs[idx].(string)] = attrs[idx+1]
    }
    return NewAttrubutes(res)
}

func (a Attributes) Bool(name string) bool {
    if v, ok := a[name]; ok {
        return v.(bool)
    }
    return false
}

func (a Attributes) Str(name string) string {
    if v, ok := a[name]; ok {
        return v.(string)
    }
    return ""
}

func (a Attributes) String() string {
    b := &bytes.Buffer{}
    par := make([]string, 0, len(a))
    fmt.Fprintf(b, "{")
    for k, v := range a {
        par = append(par, fmt.Sprintf("%s: %v", k, v))
    }
    fmt.Fprintf(b, "%s}", strings.Join(par, ", "))
    return b.String()
}
