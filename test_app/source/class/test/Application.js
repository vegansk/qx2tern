/* ************************************************************************

 Copyright:

 License:

 Authors:

 ************************************************************************ */

/**
 * This is the main application class of your custom application "test_app".
 *
 * If you have added resources to your app, remove the first '@' in the 
 * following line to make use of them.
 * @@asset(test/*)
 *
 * @ignore(environment)
 * @ignore(process)
 */
qx.Class.define("test.Application", {
        extend : qx.application.Basic,



        /*
        *****************************************************************************
        MEMBERS
        *****************************************************************************
        */

    members : {
        /**
        * This method contains the initial application code and gets called 
        * during startup of the application
        */
        main : function() {
            if (qx.core.Environment.get("runtime.name") == "rhino") {
                qx.log.Logger.register(qx.log.appender.RhinoConsole);
            } else if (qx.core.Environment.get("runtime.name") == "node.js") {
                qx.log.Logger.register(qx.log.appender.NodeConsole);
            }
            var cls = new test.TestClass();
            cls.helloFromMixin();
            cls.helloFromMixin2();
            cls.sayHello();
            this.info("Lalala");
        }
    }
});
