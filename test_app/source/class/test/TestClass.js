qx.Class.define("test.TestClass", {

        extend: qx.core.Object,

        include: [test.TestMixin, test.TestMixin2],

        construct: function() {
            this.__message = "Hello from TestClass";
        },

        members: {
            sayHello: function() {
                this.info(this.__message);
            }
        }
});
